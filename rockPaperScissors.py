import random
import time
from termcolor import colored


def rock_paper():
    rock_paper_scissors = ["rock", "paper", "scissors"]

    random_pick = random.choice(rock_paper_scissors)

    print("Lets play Rock Paper Scissors!")
    time.sleep(1)
    print("Are you ready?")
    time.sleep(1)
    user_input = int(input("""Type in 
    1. for Rock
    2. for Paper
    3. for Scissors
    """))

    user_selection = ""

    if user_input == 1:
        user_selection = "rock"
        print("Your input: " + user_selection)
        time.sleep(1)
    elif user_input == 2:
        user_selection = "paper"
        print("Your input: " + user_selection)
        time.sleep(1)
    elif user_input == 3:
        user_selection = "scissors"
        print("Your input: " + user_selection)
        time.sleep(1)

    print("Computer inputs " + random_pick)
    time.sleep(1)

    if user_selection == random_pick:
        print(colored("Tie", "blue"))
    elif user_selection == "rock" and random_pick == "paper":
        print(colored("Computer wins!", 'red'))
    elif user_selection == "rock" and random_pick == "scissors":
        print(colored("User wins!", 'green'))
    elif user_selection == "paper" and random_pick == "scissors":
        print(colored("Computer wins!", 'red'))
    elif user_selection == "paper" and random_pick == "rock":
        print(colored("User wins!", 'green'))
    elif user_selection == "scissors" and random_pick == "paper":
        print(colored("User wins!", 'green'))
    elif user_selection == "scissors" and random_pick == "rock":
        print(colored("Computer wins!", 'red'))
    else:
        print("Undefined Scenario!")

    end_selection = int(input("Do you want to play again? "
                              "Type 1 for Yes "
                              "Type 2 for No "))

    if end_selection == 1:
        rock_paper()
    else:
        exit("bye!")


play_game = int(input("do you want to play rock paper scissors? "
                      "Type 1 for Yes "
                      "Type 2 for No "))

if play_game == 1:
    rock_paper()
else:
    exit("bye!")
